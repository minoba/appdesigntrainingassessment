package nl.bioinf.appdesign.b_refactor;

/**
 * Creation date: Sep 26, 2017
 *
 * @author Michiel Noback (&copy; 2017)
 * @version 0.01
 */
public class Point {
    public final double x;
    public final double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }


}