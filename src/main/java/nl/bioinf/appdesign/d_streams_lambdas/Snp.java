package nl.bioinf.appdesign.d_streams_lambdas;

import java.util.ArrayList;
import java.util.List;

public class Snp {
    private final long position;
    private final char reference;
    private final char alternative;
    private final double minorAlleleFrequency;

    public Snp(long position, char reference, char alternative, double minorAlleleFrequency) {
        this.position = position;
        this.reference = reference;
        this.alternative = alternative;
        this.minorAlleleFrequency = minorAlleleFrequency;
    }

    public long getPosition() {
        return position;
    }

    public char getReference() {
        return reference;
    }

    public char getAlternative() {
        return alternative;
    }

    public double getMinorAlleleFrequency() {
        return minorAlleleFrequency;
    }

    @Override
    public String toString() {
        return "Snp{" +
                "position=" + position +
                ", reference=" + reference +
                ", alternative=" + alternative +
                ", minorAlleleFrequency=" + minorAlleleFrequency +
                '}';
    }

    public final static List<Snp> getSnpCollection() {
        List<Snp> snps = new ArrayList<>();
        snps.add(new Snp(100273, 'A', 'G', 0.0123));
        snps.add(new Snp(100275, 'A', 'C', 0.00323));
        snps.add(new Snp(117807, 'T', 'G', 0.1915));
        snps.add(new Snp(162889, 'C', 'G', 0.000872));
        snps.add(new Snp(190199, 'T', 'C', 0.1019));
        snps.add(new Snp(277614, 'A', 'G', 0.0168));
        snps.add(new Snp(372778, 'C', 'A', 0.0000424));
        snps.add(new Snp(417752, 'A', 'G', 1.8474e-10));
        snps.add(new Snp(478808, 'A', 'G', 1.535689e-8));
        snps.add(new Snp(556920, 'T', 'G', 0.1097));
        snps.add(new Snp(676255, 'G', 'C', 1.6672e-3));
        snps.add(new Snp(667280, 'A', 'G', 0.00287));
        snps.add(new Snp(719876, 'C', 'A', 0.006649));
        snps.add(new Snp(828771, 'A', 'C', 0.097706));
        return snps;
    }
}
