package nl.bioinf.appdesign.a_test;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Creation date: Sep 26, 2017
 *
 * @author Michiel Noback (&copy; 2017)
 * @version 0.01
 */
public class CoordinateTest {
    private static final double DELTA = 1e-15;
    private static final double ONE_DEGREE_DISTANCE = 111;

    @Test
    public void getDistanceSunny() throws Exception {
        Coordinate c1 = new Coordinate(53, 45);
        Coordinate c2 = new Coordinate(53, 46);
        double distance = c1.getDistanceEuclidean(c2);
        assertEquals(ONE_DEGREE_DISTANCE, distance, DELTA);
    }

}