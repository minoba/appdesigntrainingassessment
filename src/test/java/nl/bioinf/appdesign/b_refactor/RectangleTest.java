package nl.bioinf.appdesign.b_refactor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Creation date: Sep 26, 2017
 *
 * @author Michiel Noback (&copy; 2017)
 * @version 0.01
 */
public class RectangleTest {
    private static final double DELTA = 1e-15;
    @Test
    public void getCenter() throws Exception {
        View v = new View();
        Rectangle r = new Rectangle(v, 0, 0, 10, 20);
        Point observedP = r.getCenter();
        Point expectedP = new Point(5, 10);
        assertEquals(expectedP.x, observedP.x, DELTA);
        assertEquals(expectedP.y, observedP.y, DELTA);
    }

    @Test
    public void contains() throws Exception {
        View v = new View();
        Rectangle innerR = new Rectangle(v, 5, 8, 10, 20);
        Rectangle outerR = new Rectangle(v, 4, 7, 11, 21);
        assertTrue(outerR.contains(innerR));
        assertFalse(innerR.contains(outerR));
    }

    @Test
    public void accommodate() throws Exception {
        View v = new View();
        Rectangle innerR = new Rectangle(v, 5, 8, 10, 20);
        Rectangle outerR = new Rectangle(v, 4, 7, 11, 21);

        innerR.accommodate(outerR);
        assertTrue(innerR.contains(outerR));
    }

}